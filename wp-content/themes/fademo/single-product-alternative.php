<?php

/*
 * Template Name: Product View With Product Price
 * Template Post Type: product
 */

get_header();

echo '<strong>Template: Product View With Product Price</strong>';

if ( have_posts() ) : 
    while ( have_posts() ) : the_post(); 
        the_title( '<h2>', '</h2>' ); 
        echo "<strong>Product Price: $" . getProductPrice() . '</strong>';
        the_content();
    endwhile; 
else: 
    _e( 'Sorry, no pages matched your criteria.', 'textdomain' ); 
endif; 

get_footer();