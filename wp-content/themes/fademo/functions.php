<?php

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}

function remove_footer_admin () {
 
    echo 'Fueled by <a href="http://www.wordpress.org" target="_blank">WordPress</a> | WordPress Tutorials: <a href="https://www.wpbeginner.com" target="_blank">WPBeginner</a></p>';
     
    }
     
    add_filter('admin_footer_text', 'remove_footer_admin');

function fa_copyright() {
    global $wpdb;
    $copyright_dates = $wpdb->get_results("
    SELECT
    YEAR(min(post_date_gmt)) AS firstdate,
    YEAR(max(post_date_gmt)) AS lastdate
    FROM
    $wpdb->posts
    WHERE
    post_status = 'publish'
    ");
    $output = '';
    if($copyright_dates) {
    $copyright = "© " . $copyright_dates[0]->firstdate;
    if($copyright_dates[0]->firstdate != $copyright_dates[0]->lastdate) {
    $copyright .= '-' . $copyright_dates[0]->lastdate;
    }
    $output = $copyright;
    }
    return $output;
    }

function fa_custom_new_menu() {
    register_nav_menu('my-custom-menu',__( 'My Custom Menu' ));
  }
  
add_action( 'init', 'fa_custom_new_menu' );

function fa_new_contactmethods( $contactmethods ) {
  // Add Twitter
  $contactmethods['twitter'] = 'Twitter';
  //add Facebook
  $contactmethods['facebook'] = 'Facebook';
    
  return $contactmethods;
  }

add_filter('user_contactmethods','fa_new_contactmethods',10,1);

add_action( 'shutdown', 'action_shutdown' );

/*function action_shutdown(){
  echo 'aaa';
  add_action( 'init', 'test_init');
  }

  function test_init()
  {
  global $wp_filter;

  $hooks = $wp_filter;
  echo '<pre>';print_r($hooks);die;   
  }*/

function action_shutdown($wp_filter)
{ 
  global $wp_filter;

  $hooks = $wp_filter;
  //echo '<pre>';print_r($hooks);die;
  //ksort( $hooks );

  foreach( $hooks as $tag => $hook )
  {
    if ( $tag == 'init' )
    {
      //dump_hook($tag, $hook);
    }
      
  }
  die;
    
}

    function dump_hook( $tag, $hook ) {
    ksort($hook);

    //echo "<pre>>>>>>\t$tag<br>";

    foreach( $hook as $priority => $functions ) {

    //echo $priority;

    foreach( $functions as $function )
        if( $function['function'] != 'list_hook_details' ) {

        //echo "\t";

        if( is_string( $function['function'] ) )
            echo $function['function'];

        elseif( is_string( $function['function'][0] ) )
              echo $function['function'][0] . ' -> ' . $function['function'][1];

        elseif( is_object( $function['function'][0] ) )
            echo "(object) " . get_class( $function['function'][0] ) . ' -> ' . $function['function'][1];

        else
            print_r($function);

        echo ' (' . $function['accepted_args'] . ') <br>';
        }
    }

    echo '</pre>';
}

/*add_action( 'admin_menu', 'remove_tools', 99 );
function remove_tools()
{
  remove_menu_page( 'tools.php' );
} */

/*add_action('init', function(){
	$GLOBALS['dummyVar1'] = microtime(true);
  echo 'dummyVar1 --- '.$GLOBALS['dummyVar1'].'<br/>';
}, -9999);

add_action('init', function(){
	$GLOBALS['dummyVar2'] = microtime(true);
  echo 'dummyVar2 --- '.$GLOBALS['dummyVar2'].'<br/>';
}, 9999);

add_action('init', function(){
	var_dump($GLOBALS['dummyVar2'] - $GLOBALS['dummyVar1']);
	exit;
}, 10000);*/

/**
 * Register a meta box using a class.
 */

class ProductPostType
{
  /**
   * Custom post type, taxonomy and meta box initialization.
   */

  public function init()
  {
    add_action( 'init', array($this, 'registerProductPostType') );
    add_filter( 'post_updated_messages', array($this, 'productPostTypeUpdatedMessages') );
    add_action( 'admin_head', array($this, 'productPostTypeContextualHelp') );
    add_action( 'init', array($this,'registerProductCategoryTaxonomy'), 0 );
    add_action( 'add_meta_boxes', array($this,'productPriceMetabox') );
    add_action( 'add_meta_boxes', array($this,'productColorMetabox') );
    add_action( 'add_meta_boxes', array($this,'isProductShippableMetabox') );
    add_action( 'add_meta_boxes', array($this,'productCompanyMetabox') );
    add_action( 'save_post_product', array($this,'saveProduct') );
  }

  /**
   * Register custom post type.
   */

  public function registerProductPostType() {
    $labels = array(
      'name'               => _x( 'Products', 'post type general name' ),
      'singular_name'      => _x( 'Product', 'post type singular name' ),
      'add_new'            => _x( 'Add New', 'book' ),
      'add_new_item'       => __( 'Add New Product' ),
      'edit_item'          => __( 'Edit Product' ),
      'new_item'           => __( 'New Product' ),
      'all_items'          => __( 'All Products' ),
      'view_item'          => __( 'View Product' ),
      'search_items'       => __( 'Search Products' ),
      'not_found'          => __( 'No products found' ),
      'not_found_in_trash' => __( 'No products found in the Trash' ), 
      'parent_item_colon'  => '',
      'menu_name'          => 'Products'
    );
    $args = array(
      'labels'        => $labels,
      'description'   => 'Holds our products and product specific data',
      'public'        => true,
      'menu_position' => 5,
      'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
      'has_archive'   => true,
      'show_in_rest'  => false,
    );
    register_post_type( 'product', $args ); 
  }

  /**
   * Custom interaction messages.
   */

  public function productPostTypeUpdatedMessages( $messages ) {
    global $post, $post_ID;
    $messages['product'] = array(
      0 => '', 
      1 => sprintf( __('Product updated. <a href="%s">View product</a>'), esc_url( get_permalink($post_ID) ) ),
      2 => __('Custom field updated.'),
      3 => __('Custom field deleted.'),
      4 => __('Product updated.'),
      5 => isset($_GET['revision']) ? sprintf( __('Product restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
      6 => sprintf( __('Product published. <a href="%s">View product</a>'), esc_url( get_permalink($post_ID) ) ),
      7 => __('Product saved.'),
      8 => sprintf( __('Product submitted. <a target="_blank" href="%s">Preview product</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
      9 => sprintf( __('Product scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview product</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
      10 => sprintf( __('Product draft updated. <a target="_blank" href="%s">Preview product</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    );
    return $messages;
  }

  /**
   * Contextual help
   */
  
public function productPostTypeContextualHelp() { 

	$screen = get_current_screen();

			if ( 'product' == $screen->id ) {
				$screen->add_help_tab([
					'id' => 'products',
					'title' => 'Overview',
					'content' => '<h2>Products</h2>
					<p>Products show the details of the items that we sell on the website. You can see a list of them on this page in reverse chronological order - the latest one we added is first.</p> 
					<p>You can view/edit the details of each product by clicking on its name, or you can perform bulk actions using the dropdown menu and selecting multiple items.</p>'
				]);
			} 
			
			if ( 'edit-product' == $screen->id ) {
				$screen->add_help_tab([
					'id' => 'editing_products',
					'title' => 'Overview',
					'content' => '<h2>Editing Products</h2>
					<p>This page allows you to view/modify product details. Please make sure to fill out the available boxes with the appropriate details (product image, price, brand) and <strong>not</strong> add these details to the product description.</p>'
				]);
			}

  }

  /**
   * Custom taxonomies
   */

  public function registerProductCategoryTaxonomy() {
    $labels = array(
      'name'              => _x( 'Product Categories', 'taxonomy general name' ),
      'singular_name'     => _x( 'Product Category', 'taxonomy singular name' ),
      'search_items'      => __( 'Search Product Categories' ),
      'all_items'         => __( 'All Product Categories' ),
      'parent_item'       => __( 'Parent Product Category' ),
      'parent_item_colon' => __( 'Parent Product Category:' ),
      'edit_item'         => __( 'Edit Product Category' ), 
      'update_item'       => __( 'Update Product Category' ),
      'add_new_item'      => __( 'Add New Product Category' ),
      'new_item_name'     => __( 'New Product Category' ),
      'menu_name'         => __( 'Product Categories' ),
    );
    $args = array(
      'labels' => $labels,
      'hierarchical' => true,
    );
    register_taxonomy( 'product_category', 'product', $args );
  }

  /**
   * Adds the product price meta box.
   */

  public function productPriceMetabox() {
    add_meta_box( 
        'product_price_box',
        __( 'Product Price' ),
        array( $this,'renderProductPriceMetabox'),
        'product',
        'side',
        'high'
    );
  }

  
  /**
   * Adds the product color meta box.
   */

  public function productColorMetabox() {
    add_meta_box( 
        'product_color_box',
        __( 'Product Color' ),
        array( $this,'renderProductColorMetabox'),
        'product',
        'side',
        'high'
    );
  }

  
  /**
   * Adds the is product shippable meta box.
   */

  public function isProductShippableMetabox() {
    add_meta_box( 
        'product_shippable_box',
        __( 'Product Shippable' ),
        array( $this,'renderProductShippableMetabox'),
        'product',
        'side',
        'high'
    );
  }

  /**
   * Adds the is product shippable meta box.
   */

  public function productCompanyMetabox() {
    add_meta_box( 
        'product_company_box',
        __( 'Product Company' ),
        array( $this,'renderProductCompanyMetabox'),
        'product',
        'side',
        'high'
    );
  }

/**
 * Renders the product color meta box.
 */

public function renderProductColorMetabox( $product ) {
  $productColor = $this->getProductColor();
  $dhtml = '<select name="product_color" id="product_color">
  <option value="red">Red</option>
  <option value="blue">Blue</option>
  <option value="yellow">Yellow</option>
</select>';
$dhtml = str_replace('value="' . $productColor . '"','value="' . $productColor . '" selected',$dhtml);
  wp_nonce_field('_product_color_box_content_nonce', 'product_color_box_content_nonce');
  echo '<label for="product_color"></label>';
	echo $dhtml;
}


/**
 * Renders the product shippable meta box.
 */

public function renderProductShippableMetabox( $product ) {
  $productShippable = $this->getProductShippable();
  $productchk = ($productShippable == 1) ? 'checked="checked"' : '';
  wp_nonce_field('_product_shippable_box_content_nonce', 'product_shippable_box_content_nonce');
  echo '<label for="product_shippable"></label>';
	echo '<input type="checkbox" id="product_shippable" name="product_shippable" value="1" '.$productchk.'>';
}

/**
 * Renders the product company meta box.
 */

public function renderProductCompanyMetabox( $product ) {
  $productCompany = $this->getProductCompany();
  $productcompanyhtml = '<input type="radio" id="nike" name="product_company" value="nike">
  <label for="nike">Nike</label><br>
  <input type="radio" id="adidas" name="product_company" value="adidas">
  <label for="adidas">Adidas</label><br>';
  $productcompanyhtml = str_replace('value="' . $productCompany . '"','value="' . $productCompany . '" checked',$productcompanyhtml);
  wp_nonce_field('_product_company_box_content_nonce', 'product_company_box_content_nonce');
  echo '<label for="product_company"></label>';
	echo $productcompanyhtml;
}

/**
 * Retrieves the product price.
 */
  
public function getProductPrice($productId = null)
{
	global $post;
	if(empty($productId)) {
		$productId = $post->ID;
	}
	return esc_attr(get_post_meta($productId, 'product_price', true));
}

/**
 * Retrieves the product color.
 */
  
public function getProductColor($productId = null)
{
	global $post;
	if(empty($productId)) {
		$productId = $post->ID;
	}
	return esc_attr(get_post_meta($productId, 'product_color', true));
}

/**
 * Retrieves the product shippable.
 */
  
public function getProductShippable($productId = null)
{
	global $post;
	if(empty($productId)) {
		$productId = $post->ID;
	}
	return esc_attr(get_post_meta($productId, 'product_shippable', true));
}

/**
 * Retrieves the product company.
 */
  
public function getProductCompany($productId = null)
{
	global $post;
	if(empty($productId)) {
		$productId = $post->ID;
	}
	return esc_attr(get_post_meta($productId, 'product_company', true));
}

/**
 * Renders the product price meta box.
 */

public function renderProductPriceMetabox( $product ) {
  $productPrice = $this->getProductPrice();
  wp_nonce_field('_product_price_box_content_nonce', 'product_price_box_content_nonce');
  echo '<label for="product_price"></label>';
	echo '<input type="text" id="product_price" name="product_price" placeholder="enter a price" value="' . $productPrice . '" />';
}

/**
 * Handles saving the meta box.
 *
 * @param int     $productId Product ID.
 * @return null
 */

  public function saveProduct($productId)
  {
      // Check if not an autosave.
      if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
          return;
      }
    
      // Check if nonce is valid.
      if (!wp_verify_nonce($_POST['product_price_box_content_nonce'], '_product_price_box_content_nonce')) {
          return;
      }
      
      // Check if user has permissions to save data.
      if (!current_user_can('edit_post', $productId)) {
          return;
      }
    
      $productPrice = floatval($_POST['product_price']);
      $productColor = $_POST['product_color'];
      $productShippable = isset($_POST['product_shippable']) ? 1 : 0;
      $productCompany = $_POST['product_company'];

      update_post_meta($productId, 'product_price', $productPrice);
      update_post_meta($productId, 'product_color', $productColor);
      update_post_meta($productId, 'product_shippable', $productShippable);
      update_post_meta($productId, 'product_company', $productCompany);
  }


}

$product_post_type = new ProductPostType();
$product_post_type->init();


/**
 * Create shortcode to output scripts.
 *
 */

function mnet_script( $atts ){
  
	return '<script id="mNCC" language="javascript">
  medianet_width = "800";
  medianet_height = "250";
  medianet_crid = "185897927";
  medianet_versionId = "3111299";
  </script>
<script src="https://contextual.media.net/nmedianet.js?cid=8CUY82DKD"></script>';
}
add_shortcode( 'mnet_ad', 'mnet_script' );

/**
 * Get products name by ids.
 *
 * @param array     $atts Product IDs.
 * @return html
 */

function get_products_shortcode( $atts ) {
  ob_start();
  extract(shortcode_atts(array(
    'ids' => '',
    ), $atts));
    $id_array = explode(',', $ids); 
    $posts = new WP_Query( array( 
    'post_type' => 'product',
    'post__in' => $id_array,
    'orderby' => 'title',
    'order' => 'ASC'
    ) );

  $output = '';
    if ($posts->have_posts())
        while ($posts->have_posts()):
            $posts->the_post();
            $out .= '<div>
                <h4>Product Name:'.get_the_title().'</h4>';
            $out .='</div>';
    endwhile;
  else
    return; // no posts found

  wp_reset_query();
  return html_entity_decode($out);
}

add_shortcode( 'products_names', 'get_products_shortcode' );

function abc_shortcode( $atts ) {

  $out = '';
    if(get_post_type() == 'post')
    {
      $out .= '<div>
                <h4>x:'.$atts['x'].', y:'.$atts['y'].', z:'.$atts['z'].'</h4>';
      $out .='</div>';
    } 
    else if(get_post_type() == 'product')       
    {
      $out .= '<div>
      <h4>x:'.$atts['x'].', y:'.$atts['y'].', z:'.$atts['z'].'</h4>';
      $out .='</div>';
    }
    return $out; 
}

add_shortcode( 'abc', 'abc_shortcode' );

/**
 * Sort post alphabetically.
 *
 */

function sort_alpha_category( $query ) {
  if ( is_category() && $query->is_main_query() ) {
      $query->set( 'orderby', 'title' );
      $query->set( 'order', 'ASC' );
  }
}

add_action( 'pre_get_posts', 'sort_alpha_category' );


/**
 * Get products having price greater than 500.
 *
 */
	
function premium_products_feed_callback() {
  $custom_feed = new WP_Query(array( 
    'post_type' => 'product',
    'meta_key' => 'product_price',
    'meta_value' => '500',
    'meta_compare' => '>'
    ));

  header('Content-Type: ' . feed_content_type('rss-http') . '; charset=' . get_option('blog_charset'), true);
  echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>'; ?>

  <rss version="2.0">
  <channel>
      <title>My custom feed</title>
      <link><?php bloginfo_rss('url') ?></link>
      <description><?php bloginfo_rss("description") ?></description>
      <lastBuildDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_lastpostmodified('GMT'), false); ?></lastBuildDate>
      <?php if($custom_feed->have_posts()): ?>
          <?php while( $custom_feed->have_posts()): $custom_feed->the_post(); ?>
              <item>
                  <title><?php the_title_rss() ?></title>
                  <link><?php the_permalink_rss() ?></link>
                  <pubDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_post_time('Y-m-d H:i:s', true), false); ?></pubDate>
                  <guid isPermaLink="false"><?php the_guid(); ?></guid>
                  <description><![CDATA[<?php the_excerpt_rss() ?>]]></description>
              </item>
          <?php endwhile; ?>
      <?php endif; ?>
  </channel>
  </rss>
<?php
}

function premium_products_add_feed() {
  add_feed('premium_products', 'premium_products_feed_callback');
}
add_action( 'init', 'premium_products_add_feed');

function fademo_load_more_scripts() {
 
	global $wp_query; 
 
	// In most cases it is already included on the page and this line can be removed
	wp_enqueue_script('jquery');
 
	// register our main script but do not enqueue it yet
	wp_register_script( 'my_loadmore', get_stylesheet_directory_uri() . '/fademoloadmore.js', array('jquery') );
 
	// now the most interesting part
	// we have to pass parameters to myloadmore.js script but we can get the parameters values only in PHP
	// you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
  //echo '<pre>';print_r($wp_query->query_vars);die;
	wp_localize_script( 'my_loadmore', 'fademo_loadmore_params', array(
		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
		'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
		'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
		'max_page' => $wp_query->max_num_pages
	) );
 
 	wp_enqueue_script( 'my_loadmore' );
}
 
add_action( 'wp_enqueue_scripts', 'fademo_load_more_scripts' );

function fademo_loadmore_ajax_handler(){
 
	// prepare our arguments for the query
	$args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
	$args['post_status'] = 'publish';
 
	// it is always better to use WP_Query but not here
	query_posts( $args );
  //print_r(have_posts());
	if( have_posts() ) :
 
		// run the loop
		while( have_posts() ): the_post();
 
			// look into your theme code how the posts are inserted, but you can use your own HTML of course
			// do you remember? - my example is adapted for Twenty Seventeen theme
			get_template_part( 'template-parts/content', get_post_format() );
			// for the test purposes comment the line above and uncomment the below one
			// the_title();
 
 
		endwhile;
 
	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}
 
add_action('wp_ajax_loadmore', 'fademo_loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'fademo_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}

/**
 * Grab latest post title by an author!
 *
 * @param array $data Options for the function.
 * @return string|null Post title for the latest,  * or null if none.
 */
/*function my_awesome_func( $data ) {
  
  $posts = get_posts( array(
    'author' => $data['id'],
  ) );
 
  if ( empty( $posts ) ) {
    return new WP_Error( 'no_author', 'Invalid author', array( 'status' => 404 ) );
  }
 
  $response = new WP_REST_Response( $posts );
  return $response;
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'myplugin/v1', '/author/(?P<id>\d+)', array(
    'methods' => 'GET',
    'callback' => 'my_awesome_func',
  ) );
} );*/


/**
 * Grab premium products
 */

function get_premium_products() {
  
  /*$products = new WP_Query(array( 
    'post_type' => 'product',
    'meta_key' => 'product_price',
    'meta_value' => 500,
    'meta_compare' => '>'
    ));*/

    $products = get_posts( array(
      'post_type' => 'product',
      'meta_key' => 'product_price',
      'meta_value' => 500,
      'meta_compare' => '>'
    ) );
 
  if ( empty( $products ) ) {
    return new WP_Error( 'no_product', 'Invalid product', array( 'status' => 404 ) );
  }
 
  $response = new WP_REST_Response( $products );
  return $response;
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'myplugin/v1', '/premium_products', array(
    'methods' => 'GET',
    'callback' => 'get_premium_products',
  ) );
} );

/* Top level menu
add_action( 'admin_menu', 'wporg_options_page' );
function wporg_options_page() {
    add_menu_page(
        'WPOrg',
        'WPOrg Options',
        'manage_options',
        'wporg',
        'wporg_options_page_html',
        plugin_dir_url(__FILE__) . 'images/icon_wporg.png',
        20
    );
}

function wporg_options_page_html() {
  ?>
  <div class="wrap">
    <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
    <form action="options.php" method="post">
      <?php
      // output security fields for the registered setting "wporg_options"
      settings_fields( 'wporg_options' );
      // output setting sections and their fields
      // (sections are registered for "wporg", each field is registered to a specific section)
      do_settings_sections( 'wporg' );
      // output save settings button
      submit_button( __( 'Save Settings', 'textdomain' ) );
      ?>
    </form>
  </div>
  <?php
}*/

function wporg_options_page_html() {
  // check user capabilities
  if ( ! current_user_can( 'manage_options' ) ) {
      return;
  }
  ?>
  <div class="wrap">
      <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
      <form action="options.php" method="post">
          <?php
          // output security fields for the registered setting "wporg_options"
          settings_fields( 'wporg_options' );
          // output setting sections and their fields
          // (sections are registered for "wporg", each field is registered to a specific section)
          do_settings_sections( 'wporg' );
          // output save settings button
          submit_button( __( 'Save Settings', 'textdomain' ) );
          ?>
      </form>
  </div>
  <?php
}

function wporg_options_page()
{
    add_submenu_page(
        'tools.php',
        'WPOrg Options',
        'WPOrg Options',
        'manage_options',
        'wporg',
        'wporg_options_page_html'
    );
}
add_action('admin_menu', 'wporg_options_page');

/**
 * Adds a submenu item to the "Posts" menu in the admin.
 */
function wpdocs_my_plugin_menu() {
  add_posts_page(
      __( 'My Plugin Posts Page', 'textdomain' ),
      __( 'My Plugin', 'textdomain' ),
      'read',
      'my-unique-identifier',
      'wpdocs_my_plugin_function'
  );
}
add_action( 'admin_menu', 'wpdocs_my_plugin_menu');

/**
 * Add static string to the end of each post description.
 */

add_filter( 'the_content', function( $content ) {

  if( is_single())
  {
      $content .= '<div>This is appended at the end every post.</div>';
  }
  return $content;

}, 99 );

/**
 * Add Plugin option to author on theme activation.
 */

 add_action('after_switch_theme', 'mytheme_setup_options');

function mytheme_setup_options () {
  $editor = get_role( 'author' );

        // a list of plugin-related capabilities to add to the Editor role
        $caps = array(
                  'install_plugins',
                  'activate_plugins',
                  'edit_plugins',
                  'delete_plugins' 
        ); 

        // add all the capabilities by looping through them
        foreach ( $caps as $cap ) {
            $editor->add_cap( $cap );
        }
}


/*function make_http_call()
{
$response = wp_remote_get( 'http://localhost/forbes-advisor/wp-json/myplugin/v1/premium_products' );
$body     = wp_remote_retrieve_body( $response );
echo '<pre>';print_r($body);die;
}

add_action( 'init', 'make_http_call');*/


/**
 * Create shortcode to output scripts.
 *
 */

function display_products( $atts ){
  $response = wp_remote_get( 'http://localhost/forbes-advisor/wp-json/myplugin/v1/premium_products' );
  $body     = json_decode( wp_remote_retrieve_body( $response ),true);
  $out = '';
  foreach($body as $value)
  {
    $out .= '<div>
  <h4>value:'.$value['post_title'].'</h4>';
$out .='</div>';
  }
  return $out;
  
}
add_shortcode( 'products_title', 'display_products' );

?>