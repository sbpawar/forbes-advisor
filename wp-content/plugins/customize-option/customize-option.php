<?php

/*
Plugin Name:  Customize Option
Version:  1.0
Description:  Demonstrating how to customize WordPress User Role
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  customize-option
*/

function change_title() {
	$change_title = 'Forbes Wheel';
	$site_title = get_option( 'blogname' );

	// Save "normal title" if not currently joke title
	if( $site_title !== $change_title ) {
		update_option( 'site_normal_title', $site_title );
	}

	// On April 1, set site title to joke title
	$day = date( 'F j' );
	if( $day === 'June 6' ) {	
		update_option( 'blogname', $change_title );
		return;
	}

	// If normal_title exists and the site title's the joke title, change it back
	$normal_title = get_option( 'site_normal_title' );
	if ( $site_title === $change_title && $normal_title ) {
		update_option( 'blogname', $normal_title );
	}
}
add_action( 'init', 'change_title' );


/**
 * Transient to store template for 5 minutes
 */

add_filter( 'template_include', 'var_template_include');

function var_template_include( $t ){
    // Get any existing copy of our transient data
    if ( false === ( $customize_option_detect_template = get_transient( 'customize_option_detect_template' ) ) ||  get_transient( 'customize_option_detect_template' ) != basename($t)) {
        set_transient( 'customize_option_detect_template', basename($t), 300 );
    }
    return $t;
}


/**
 * Add static string to the end of each post description.
 */

/*add_filter( 'the_content', function( $content ) {

    if( is_single())
    {
        global $wpdb;
        //$user_count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->users" );
        $postdata = $wpdb->get_row("SELECT * FROM $wpdb->postmeta WHERE meta_key ='product_color' and meta_value='yellow'");
        $content .= '<div>User Count:'.print_r($postdata).'</div>';
    }
    return $content;
  
  });*/
  