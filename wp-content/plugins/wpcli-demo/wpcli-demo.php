<?php
/*
Plugin Name: wpcli demo
Plugin URI: http://localhost/forbes-advisor
Description: Get last published post
Author: Shyamrao Pawar
Version: 1.0.0
Author URI: http://localhost/forbes-advisor
*/

class WPCLIDemo_CLI {

	/**
	 * Returns 'Last Published Post'
	 */
	public function get_last_published() {
        
        $args = array( 'numberposts' => 1 );
        $recent_posts = wp_get_recent_posts( $args );
        foreach( $recent_posts as $recent ){
            $template_path = get_post_meta($recent["ID"], '_wp_page_template', true);
            $templates = wp_get_theme()->get_page_templates();
            WP_CLI::line( 'Post id: '.$recent["ID"] );
            WP_CLI::line( 'Post title: '.$recent["post_title"] );
            WP_CLI::line( 'Post Content: '.$recent["post_content"] );
            WP_CLI::line( 'Post template: '. $templates[$template_path] );
        }
        
	}

}

/**
 * Registers our command when cli get's initialized.
 */
function wp_cli_register_commands() {
	WP_CLI::add_command( 'last-published-post', 'WPCLIDemo_CLI' );
}

add_action( 'cli_init', 'wp_cli_register_commands' );