<?php
class Sal_Customize_User_Role {
    public static function activate() {
        // get the Editor role's object from WP_Role class
        $editor = get_role( 'contributor' );

        // a list of plugin-related capabilities to add to the Editor role
        $caps = array(
                  'install_plugins',
                  'activate_plugins',
                  'edit_plugins',
                  'delete_plugins' 
        ); 

        // add all the capabilities by looping through them
        foreach ( $caps as $cap ) {
            $editor->add_cap( $cap );
        }
    }
}