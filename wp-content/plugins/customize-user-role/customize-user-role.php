<?php
/*
Plugin Name:  Customize User Role
Version:  1.0
Description:  Demonstrating how to customize WordPress User Role
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  customize-user-role
*/

// this code runs only during plugin activation and never again
function sal_customize_user_role() {
    require_once plugin_dir_path( __FILE__ ).'includes/class-sal-customize-user-role.php';   
    Sal_Customize_User_Role::activate();
}
register_activation_hook( __FILE__, 'sal_customize_user_role' );