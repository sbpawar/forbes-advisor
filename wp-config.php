<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'forbes_advisor' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'PjS|ywtvx-0`A05 4AwbgyUF,om0t0@JP3o*UT_%4gies[9C?SB1pkyMx,K<rn[H' );
define( 'SECURE_AUTH_KEY',  '*%~+g3dpI57y-w?jsu/J!=.QA@@pj4G@pG=l2FHeN00g9(6_7P[Mbk,S}k#WmK[~' );
define( 'LOGGED_IN_KEY',    ' EWp(4GCwf.3Y#{-v7u0~5Fm+GoEKX#)0bhC?w3V&m),Ku+zmegRf$_#xun(Q#=5' );
define( 'NONCE_KEY',        'o,;N;t]osZVLT$Jk6o3a]ICZ)+2;0@Vin+vDBLg*;V+Pui,!f=>2/PGasK#hL0iC' );
define( 'AUTH_SALT',        'SqyJ0XWx`_ygt9YatOQu}F@m~%QkQORu|4-`?jQQZDWAr=p}YR|]PZr2#8%$?=nR' );
define( 'SECURE_AUTH_SALT', '$~:K2=b`%l!a-kH{K1@2L.;OCk{d&6F*;@c9a.~3x*!e@fDQ?kK6:JHi_(AWs3jq' );
define( 'LOGGED_IN_SALT',   'e>qa{EW DpZ&dW#j?zJqAUp!DZCu9bh233&A>-fB(?Qqa%qnzTGusevx~zn9=>`u' );
define( 'NONCE_SALT',       'ga*f[Wg|P(j-RNeu1Qb^LE-;&#[jp@(kHauhH,$;/2xn-;<I.$FXFDp4prdG8a;q' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', true );

// Disable display of errors and warnings
define( 'WP_DEBUG_DISPLAY', false );
@ini_set( 'display_errors', 0 );

// Use dev versions of core JS and CSS files
define( 'SCRIPT_DEBUG', true );

define( 'SAVEQUERIES', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
